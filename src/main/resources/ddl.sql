CREATE TABLE subscriber
(
  id        BIGINT           NOT NULL
    CONSTRAINT user_pkey
    PRIMARY KEY,
  name      VARCHAR(20)      NOT NULL,
  surname   VARCHAR(20)      NOT NULL,
  amount    DOUBLE PRECISION NOT NULL,
  tariff_id BIGINT
);

CREATE TABLE tariff
(
  id          BIGINT           NOT NULL
    CONSTRAINT tariff_pkey
    PRIMARY KEY,
  name        VARCHAR(20)      NOT NULL,
  price       DOUBLE PRECISION NOT NULL,
  description VARCHAR(255)
);

