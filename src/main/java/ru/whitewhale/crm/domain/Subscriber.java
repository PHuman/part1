package ru.whitewhale.crm.domain;

import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Entity
@Table(name = "subscriber")
@XmlRootElement
public class Subscriber {
    @Id
    private Long id;
    private String name;
    private String surname;
    private Double amount;

    @ManyToOne
    @JoinColumn(name = "tariff_id")
    private TariffPlan tariffPlan;
}
