package ru.whitewhale.crm.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Entity
@Table(name = "tariff")
@XmlRootElement
public class TariffPlan {
    @Id
    private Long id;
    private String name;
    private String description;
    private double price;
}
