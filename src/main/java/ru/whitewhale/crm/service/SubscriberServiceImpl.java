package ru.whitewhale.crm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.whitewhale.crm.domain.Subscriber;
import ru.whitewhale.crm.repository.SubscriberRepository;

import java.util.List;

@Service
public class SubscriberServiceImpl implements SubscriberService {
    private final SubscriberRepository repository;

    @Autowired
    public SubscriberServiceImpl(SubscriberRepository repository) {
        this.repository = repository;
    }

    public List<Subscriber> getAllSubscribers() {
        return repository.findAll();
    }
}
