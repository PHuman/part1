package ru.whitewhale.crm.service;

import ru.whitewhale.crm.domain.Subscriber;

import java.util.List;

public interface SubscriberService {

    List<Subscriber> getAllSubscribers();
}
