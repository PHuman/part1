package ru.whitewhale.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.whitewhale.crm.domain.Subscriber;

public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {
}
