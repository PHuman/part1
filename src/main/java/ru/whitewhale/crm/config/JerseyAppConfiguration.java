package ru.whitewhale.crm.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import ru.whitewhale.crm.rest.SubscriberRestService;

import javax.servlet.ServletContext;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Context;

@ApplicationPath("/")
public class JerseyAppConfiguration extends ResourceConfig {

    public JerseyAppConfiguration(@Context ServletContext context) {
        WebApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(context);

        register(appCtx.getBean(SubscriberRestService.class));
    }

}
