package ru.whitewhale.crm.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.whitewhale.crm.domain.Subscriber;
import ru.whitewhale.crm.service.SubscriberService;

import java.util.List;

@Service
public class SubscriberRestServiceImpl implements SubscriberRestService {
    private final SubscriberService service;

    @Autowired
    public SubscriberRestServiceImpl(SubscriberService service) {
        this.service = service;
    }

    @Override
    public List<Subscriber> getAllSubscribersInJson() {
        return service.getAllSubscribers();
    }

    @Override
    public List<Subscriber> getAllSubscribersInXml() {
        return service.getAllSubscribers();
    }
}
