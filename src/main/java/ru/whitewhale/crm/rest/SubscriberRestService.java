package ru.whitewhale.crm.rest;

import ru.whitewhale.crm.domain.Subscriber;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;

@Path("/subscribers")
public interface SubscriberRestService {

    @GET
    @Path("/json")
    @Produces(APPLICATION_JSON)
    List<Subscriber> getAllSubscribersInJson();

    @GET
    @Path("/xml")
    @Produces(APPLICATION_XML)
    List<Subscriber> getAllSubscribersInXml();
}
