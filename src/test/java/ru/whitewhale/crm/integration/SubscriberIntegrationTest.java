package ru.whitewhale.crm.integration;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import ru.whitewhale.crm.config.JerseyAppConfiguration;
import ru.whitewhale.crm.config.SpringAppConfiguration;
import ru.whitewhale.crm.domain.Subscriber;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.LogManager;

import static junit.framework.TestCase.assertFalse;

public class SubscriberIntegrationTest extends JerseyTest {

    @Override
    public Application configure() {
        LogManager.getLogManager().reset();

        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(SpringAppConfiguration.class);
        context.refresh();

        MockServletContext mockServletContext = new MockServletContext();
        mockServletContext.setAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, context);

        JerseyAppConfiguration config = new JerseyAppConfiguration(mockServletContext);
        config.property("contextConfig", context);
        return config;
    }

    @Test
    public void testGetAllSubscribersInJson() {
        Response response = target("/subscribers/json").request().get();

        List<Subscriber> resp = response.readEntity(List.class);

        assertFalse(resp.isEmpty());

        response.close();
    }

    @Test
    public void testGetAllSubscribersInXml() {
        Response response = target("/subscribers/xml").request().get();

        String resp = response.readEntity(String.class);
        assertFalse(resp.isEmpty());

        response.close();
    }
}