package ru.whitewhale.crm.integration.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.whitewhale.crm.domain.Subscriber;
import ru.whitewhale.crm.rest.SubscriberRestServiceImpl;
import ru.whitewhale.crm.service.SubscriberService;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class SubscriberRestServiceImplTest {

    @Mock
    private SubscriberService service;

    @InjectMocks
    private SubscriberRestServiceImpl restService;

    @Test
    public void shouldReturnAllSubscribers() {
        Subscriber subscriber = new Subscriber();
        List<Subscriber> subscribers = singletonList(subscriber);
        given(service.getAllSubscribers()).willReturn(subscribers);

        List<Subscriber> allSubscribers = restService.getAllSubscribersInJson();

        assertEquals(allSubscribers, subscribers);
    }
}
