package ru.whitewhale.crm.integration.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.whitewhale.crm.domain.Subscriber;
import ru.whitewhale.crm.repository.SubscriberRepository;
import ru.whitewhale.crm.service.SubscriberServiceImpl;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class SubscriberServiceImplTest {

    @Mock
    private SubscriberRepository repository;

    @InjectMocks
    private SubscriberServiceImpl service;

    @Test
    public void shouldReturnAllSubscribers() {
        Subscriber subscriber = new Subscriber();
        List<Subscriber> subscribers = singletonList(subscriber);
        given(repository.findAll()).willReturn(subscribers);

        List<Subscriber> allSubscribers = service.getAllSubscribers();

        assertEquals(allSubscribers, subscribers);
    }
}
